from django.urls import path
from .views import home, clientes, servicios, contacto, base


urlpatterns = [
    path('', home, name="home"),
    path('clientes/', clientes, name="clientes"),
    path('servicios/', servicios, name="servicios"),
    path('contacto/', contacto, name="contacto"),
    path('base/', base, name="base"),
]