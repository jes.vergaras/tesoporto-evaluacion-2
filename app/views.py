from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'app/home.html')

def clientes(request):
    return render(request, 'app/clientes.html')

def servicios(request):
    return render(request, 'app/servicios.html')

def contacto(request):
    return render(request, 'app/contacto.html')

def base(request):
    return render(request, 'app/base.html')
